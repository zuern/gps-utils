// Copyright 2022 Kevin Zuern. All rights reserved.

package main

import (
	"embed"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"

	"github.com/hashicorp/go-retryablehttp"
	"gitlab.com/zuern/log"
)

//go:embed static
var staticFiles embed.FS

type Server struct {
	httpClient *retryablehttp.Client
}

func NewServer(logger log.Logger) *Server {
	s := &Server{
		httpClient: retryablehttp.NewClient(),
	}
	s.httpClient.RetryMax = 1
	s.httpClient.Logger = NewHTTPClientLogger(logger)
	return s
}

func (s *Server) HandleIndex() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		index, err := staticFiles.Open("static/index.html")
		if err != nil {
			panic(err)
		}
		defer index.Close()
		io.Copy(res, index)
	}
}

func (s *Server) HandleStatic() http.Handler {
	return http.FileServer(http.FS(staticFiles))
}

func (s *Server) HandleOSMtoKML() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		if err := req.ParseForm(); err != nil {
			http.Error(res, err.Error(), http.StatusBadRequest)
			return
		}
		url, ok := req.Form["url"]
		if !ok {
			http.Error(res, "url not in form data", http.StatusBadRequest)
			return
		}
		osmRoot, err := DownloadOSMWay(s.httpClient, url[0])
		if err != nil {
			http.Error(res, fmt.Sprintf("Failed to get way: %v", err), http.StatusInternalServerError)
			return
		}
		kml, err := osmRoot.ConvertToKML()
		if err != nil {
			logger := log.FromContext(req.Context())
			logger.Error("Failed to convert OSM doc to KML", log.Err(err))
			code := http.StatusInternalServerError
			http.Error(res, http.StatusText(code), code)
			return
		}
		filename := kml.Document.Name + ".kml"
		res.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=%q", filename))
		if err := xml.NewEncoder(res).Encode(kml); err != nil {
			logger := log.FromContext(req.Context())
			logger.Error("Failed to encode KML doc as XML", log.Err(err))
		}
	}
}
