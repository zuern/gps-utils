// Copyright 2022 Kevin Zuern. All rights reserved.

package main

import (
	"github.com/gorilla/mux"

	"gitlab.com/zuern/app/middleware"
	"gitlab.com/zuern/log"
)

func registerRoutes(server *Server, r *mux.Router, logger log.Logger) {
	r.Use(middleware.PopulateRequestReplyLogger(logger.WithOptions(log.Name("accesslog"))))

	r.Path("/").HandlerFunc(server.HandleIndex())
	r.PathPrefix("/static").Handler(server.HandleStatic())

	r.Methods("POST").Path("/osm-to-kml").HandlerFunc(server.HandleOSMtoKML())
}
