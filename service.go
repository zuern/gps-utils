// Copyright 2022 Kevin Zuern. All rights reserved.

package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"strings"

	retryablehttp "github.com/hashicorp/go-retryablehttp"
	"gitlab.com/zuern/geotool/model/osm"
)

// download the contents of the url and return as bytes.
func downloadSync(httpClient *retryablehttp.Client, url string) ([]byte, error) {
	resp, err := httpClient.Get(url)
	if err != nil {
		return nil, fmt.Errorf("could not get url %v: %w", url, err)
	}
	defer resp.Body.Close()
	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("could not read response from url %v: %w", url, err)
	}
	return bytes, err
}

func DownloadOSMWay(httpClient *retryablehttp.Client, url string) (*osm.OSM, error) {
	const osmWayPath = "https://www.openstreetmap.org/api/0.6/way/"
	if !strings.HasPrefix(url, osmWayPath) {
		return nil, fmt.Errorf("invalid url did not start with %v", osmWayPath)
	}
	const full = "/full"
	if !strings.HasSuffix(url, full) {
		url += full
	}
	wayBytes, err := downloadSync(httpClient, url)
	if err != nil {
		return nil, err
	}
	var root osm.OSM
	if err = xml.Unmarshal(wayBytes, &root); err != nil {
		return nil, fmt.Errorf("unmarshal OSM document: %w", err)
	}
	return &root, nil
}
