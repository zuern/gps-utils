FROM alpine as builder

RUN apk add ca-certificates tzdata && update-ca-certificates

FROM scratch

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY geotool /usr/bin/geotool
ENTRYPOINT ["/usr/bin/geotool"]
