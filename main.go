// Copyright 2022 Kevin Zuern. All rights reserved.

package main

import (
	"context"
	"os"

	"gitlab.com/zuern/app"
	"gitlab.com/zuern/cli"
)

const (
	appName   = "geotool"
	appDesc   = "website with geo-related tools"
	appUsage  = "geotool [OPTIONS]"
	envPrefix = "GEOTOOL_"
)

func main() {
	os.Exit(app.New().Run(
		appName,
		appDesc,
		appUsage,
		envPrefix,
		[]cli.Opt{},
		[]cli.Arg{},
		setup,
	))
}

func setup(geotool *app.App) (shutdownFunc func(context.Context) error, err error) {
	s := NewServer(geotool.Log)
	registerRoutes(s, geotool.Router, geotool.Log)
	return nil, nil
}
