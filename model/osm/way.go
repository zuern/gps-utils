// Copyright 2022 Kevin Zuern. All rights reserved.

package osm

type Way struct {
	ID       string    `xml:"id,attr"`
	NodeRefs []NodeRef `xml:"nd"`
	Tags     []Tag     `xml:"tag"`
}
