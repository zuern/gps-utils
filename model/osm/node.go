// Copyright 2022 Kevin Zuern. All rights reserved.

package osm

type Node struct {
	ID  string `xml:"id,attr"`
	Lat string `xml:"lat,attr"`
	Lon string `xml:"lon,attr"`
}
