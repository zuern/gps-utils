// Copyright 2022 Kevin Zuern. All rights reserved.

package osm

type NodeRef struct {
	Ref string `xml:"ref,attr"`
}
