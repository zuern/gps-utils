// Copyright 2022 Kevin Zuern. All rights reserved.

package osm

import (
	"fmt"
	"strings"

	"gitlab.com/zuern/geotool/model/kml"
)

type OSM struct {
	Copyright   string `xml:"copyright,attr"`
	License     string `xml:"license,attr"`
	Attribution string `xml:"attribution,attr"`
	Nodes       []Node `xml:"node"`
	Way         Way    `xml:"way"`
}

func (o OSM) ConvertToKML() (*kml.KML, error) {
	const lineColor = "ff0000ff"
	const lineWidth = 4
	const styleID = "style1"

	name := fmt.Sprintf("OpenStreetMap Way ID %s", o.Way.ID)
	var descParts []string
	if attribution := o.Attribution; attribution != "" {
		descParts = append(descParts, "Attribution: "+attribution)
	}

	if copyright := o.Copyright; copyright != "" {
		descParts = append(descParts, "Copyright: "+copyright)
	}

	if license := o.License; license != "" {
		descParts = append(descParts, "License: "+license)
	}

	description := strings.Join(descParts, "<br>")

	nodes := map[string]Node{}
	for _, node := range o.Nodes {
		nodes[node.ID] = node
	}

	var coordinates []kml.Coordinate
	for _, nodeRef := range o.Way.NodeRefs {
		ref := nodeRef.Ref
		node, ok := nodes[ref]
		if !ok {
			return nil, fmt.Errorf("cannot convert OSM Way to KML: node reference %s not found in the document", ref)
		}
		coordinates = append(coordinates, kml.Coordinate{Lat: node.Lat, Lon: node.Lon})
	}

	doc := kml.Document{
		Name: name,
		Style: kml.Style{
			ID: styleID,
			LineStyle: kml.LineStyle{
				Color: lineColor,
				Width: lineWidth,
			},
		},
		Placemark: kml.Placemark{
			Name:        name,
			Description: description,
			StyleURL:    styleID,
			MultiGeometry: kml.MultiGeometry{
				LineString: kml.LineString{
					Tesselate:   true,
					Coordinates: coordinates,
				},
			},
		},
	}
	kmlRoot := &kml.KML{
		Document: doc,
	}
	return kmlRoot, nil
}
