// Copyright 2022 Kevin Zuern. All rights reserved.

package osm

type Tag struct {
	Key   string `xml:"k,attr"`
	Value string `xml:"v,attr"`
}
