// Copyright 2022 Kevin Zuern. All rights reserved.

package kml

type Style struct {
	ID        string    `xml:"id,attr"`
	LineStyle LineStyle `xml:"LineStyle"`
}
