// Copyright 2022 Kevin Zuern. All rights reserved.

package kml

import (
	"encoding/xml"
)

type KML struct {
	Document Document
}

func (k KML) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	start.Name.Local = "kml"
	start.Attr = append(start.Attr, xml.Attr{
		Name:  xml.Name{Local: "xmlns:kml"},
		Value: "http://www.opengis.net/kml/2.2",
	})
	e.EncodeToken(start)
	doc := xml.StartElement{Name: xml.Name{Local: "Document"}}
	if err := e.EncodeElement(k.Document, doc); err != nil {
		return err
	}
	return e.EncodeToken(start.End())
}
