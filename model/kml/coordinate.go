// Copyright 2022 Kevin Zuern. All rights reserved.

package kml

type Coordinate struct {
	Lat string
	Lon string
}
