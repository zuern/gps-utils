// Copyright 2022 Kevin Zuern. All rights reserved.

package kml

import "encoding/xml"

type Placemark struct {
	Name          string
	Description   string
	StyleURL      string
	MultiGeometry MultiGeometry
}

func (p Placemark) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	type Desc struct {
		Text string `xml:",cdata"`
	}
	type placemark struct {
		Name          string        `xml:"name"`
		Description   Desc          `xml:"description"`
		StyleURL      string        `xml:"styleUrl"`
		MultiGeometry MultiGeometry `xml:"MultiGeometry"`
	}
	data := &placemark{
		Name:          p.Name,
		Description:   Desc{p.Description},
		StyleURL:      p.StyleURL,
		MultiGeometry: p.MultiGeometry,
	}
	return e.EncodeElement(data, start)
}
