// Copyright 2022 Kevin Zuern. All rights reserved.

package kml

type LineStyle struct {
	Color string `xml:"color"`
	Width int    `xml:"width"`
}
