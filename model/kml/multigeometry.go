// Copyright 2022 Kevin Zuern. All rights reserved.

package kml

type MultiGeometry struct {
	LineString LineString `xml:"LineString"`
}
