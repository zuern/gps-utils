// Copyright 2022 Kevin Zuern. All rights reserved.

package kml

type Document struct {
	Name      string    `xml:"name"`
	Style     Style     `xml:"Style"`
	Placemark Placemark `xml:"Placemark"`
}
