// Copyright 2022 Kevin Zuern. All rights reserved.

package kml

import (
	"encoding/xml"
	"strings"
)

// https://developers.google.com/kml/documentation/kmlreference#linestring
type LineString struct {
	Tesselate   bool
	Coordinates []Coordinate
}

func (l LineString) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	var coordinates strings.Builder
	for i, c := range l.Coordinates {
		coordinates.WriteString(c.Lon)
		coordinates.WriteRune(',')
		coordinates.WriteString(c.Lat)
		if i >= 0 && i < len(l.Coordinates)-1 {
			coordinates.WriteRune(' ')
		}
	}
	e.EncodeToken(start)

	elemTesselate := xml.StartElement{Name: xml.Name{Local: "tesselate"}}
	var tesselate int8
	if l.Tesselate {
		tesselate = 1
	}
	if err := e.EncodeElement(tesselate, elemTesselate); err != nil {
		return err
	}

	elem := xml.StartElement{Name: xml.Name{Local: "coordinates"}}
	if err := e.EncodeToken(elem); err != nil {
		return err
	}
	if err := e.EncodeToken(xml.CharData(coordinates.String())); err != nil {
		return err
	}
	if err := e.EncodeToken(elem.End()); err != nil {
		return err
	}
	return e.EncodeToken(start.End())
}
