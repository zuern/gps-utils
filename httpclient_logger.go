// Copyright 2022 Kevin Zuern. All rights reserved.

package main

import (
	"fmt"
	"net/url"

	"gitlab.com/zuern/log"
)

type HTTPClientLogger struct {
	Log log.Logger
}

func NewHTTPClientLogger(logger log.Logger) *HTTPClientLogger {
	return &HTTPClientLogger{
		Log: logger.WithOptions(log.CallerSkip(1)),
	}
}

func (h HTTPClientLogger) convertKV(kvs []any) []log.Field {
	var fields []log.Field
	var key string
	var val any
	for i, x := range kvs {
		if i%2 == 0 {
			key = x.(string)
			continue
		}
		val = x
		var field log.Field
		switch v := val.(type) {
		case string:
			field = log.Str(key, v)
		case bool:
			field = log.Bool(key, v)
		case error:
			field = log.NamedError(key, v)
		case int:
			field = log.Int(key, int64(v))
		case *url.URL:
			field = log.Str(key, v.String())
		default:
			panic(fmt.Sprintf("unhandled value type %T", val))
		}
		fields = append(fields, field)
	}
	return fields
}

func (h HTTPClientLogger) Error(msg string, keysAndValues ...interface{}) {
	h.Log.With(h.convertKV(keysAndValues)...).Error(msg)
}

func (h HTTPClientLogger) Info(msg string, keysAndValues ...interface{}) {
	h.Log.With(h.convertKV(keysAndValues)...).Info(msg)
}

func (h HTTPClientLogger) Debug(msg string, keysAndValues ...interface{}) {
	h.Log.With(h.convertKV(keysAndValues)...).Debug(msg)
}

func (h HTTPClientLogger) Warn(msg string, keysAndValues ...interface{}) {
	h.Log.With(h.convertKV(keysAndValues)...).Warn(msg)
}
