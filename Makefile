geotool: *.go
	CGO_ENABLED=0 GOOS=linux go build .

docker: geotool Dockerfile
	docker build . --tag geotool

docker-push: docker
	docker tag geotool registry.gitlab.com/zuern/geotool/geotool:latest
	docker push registry.gitlab.com/zuern/geotool/geotool:latest

release:
	goreleaser release
