GeoTool
=========

https://geotool.earth

Contains utilities for manipulating GPS / Geographic data.

## How to run

Locally:

```ShellSession
go run .
```

In Docker:

```ShellSession
make docker
docker run -p 3000:3000 geotool
```
